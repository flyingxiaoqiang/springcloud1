package cn.tedu.sp02.controller;

import java.util.List;
import java.util.Random;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.service.ItemService;
import cn.tedu.web.util.JsonResult;

@Slf4j
@RestController
public class ItemController {
    @Autowired
    private ItemService itemService;

    @Value("${server.port}")
    private int port;

    @GetMapping("/{orderId}")
    public JsonResult<List<Item>> getItems(@PathVariable String orderId) throws InterruptedException {
        log.info("server.port="+port+", orderId="+orderId);
        List<Item> items = itemService.getItems(orderId);

        //随机的延迟代码
        //90%概率会执行随机延迟代码
        if(Math.random()<0.9){
            //随机延迟时长0-5s
            int t = new Random().nextInt(5000);
            log.info("随机延迟:"+t);
            Thread.sleep(t);
        }
        return JsonResult.ok(items).msg("port="+port);
    }

    /***
     * @RequestBody
     * 接收客户端提交的参数
     * 完整的接收http协议体数据，再转成java数据
     * @param items
     * @return
     */
    @PostMapping("/decreaseNumber")
    public JsonResult<?> decreaseNumber(@RequestBody List<Item> items) {
        itemService.decreaseNumbers(items);
        return JsonResult.ok().msg("减少商品库存成功");
    }
}
