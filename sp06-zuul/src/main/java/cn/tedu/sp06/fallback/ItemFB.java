package cn.tedu.sp06.fallback;

import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
public class ItemFB implements FallbackProvider {

    //针对那个服务应用当前降级类
    //返回服务id
    //    item-service - 只针对商品服务降级
    //    * - 对所有服务进行降级
    //    null - 对所有服务进行降级
    @Override
    public String getRoute() {
        return "item-service";
    }

    // 返回的降级响应，封装在response对象中
    // 根据自己应用的需求，可以返回任意的响应数据
    //    - 错误提示
    //    - 缓存的数据
    //    - 执行业务运算，返回结果
    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        return new TestClass();
    }
}
