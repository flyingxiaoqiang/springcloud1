package cn.tedu.sp06.fallback;

import cn.tedu.web.util.JsonResult;
import com.fasterxml.jackson.annotation.JsonRawValue;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class TestClass implements ClientHttpResponse {
    @Override
    public HttpStatus getStatusCode() throws IOException {
        return HttpStatus.INTERNAL_SERVER_ERROR; // 500 , internal error
    }

    @Override
    public int getRawStatusCode() throws IOException {
        return HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

    @Override
    public String getStatusText() throws IOException {
        return HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
    }

    @Override
    public void close() {
        // BAIS 流时内存数组的流，不占用底层系统资源
        // 不需要 close() 释放资源
    }

    @Override
    public InputStream getBody() throws IOException {
        //返回JsonResult----{code:500,msg:"系统服务故障",data:null}
        String json = JsonResult.err().code(500).msg("系统服务故障").toString();

        return new ByteArrayInputStream(json.getBytes("UTF-8"));
    }

    @Override
    public HttpHeaders getHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json;charset=UTF-8");
        return httpHeaders;
    }
}
