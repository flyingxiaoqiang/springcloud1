package cn.tedu.sp06.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AccessFilter extends ZuulFilter {
    // 设置过滤器类型：pre、routing、prst、error
    @Override
    public String filterType() {
//        return "pre";
        return FilterConstants.PRE_TYPE;
    }

    // 位置序号
    @Override
    public int filterOrder() {
        // 第五个过滤器在上下文对象中放入了serviceId
        // 后面过滤器才能访问
        return 6;
    }

    // 判断针对当前请求是否执行过滤代码
    @Override
    public boolean shouldFilter() {
        /***
         * 调用商品判断权限
         * 调用用户和订单不检查权限
         */
        // 获得调用的服务id，currentContext-键值对格式
        RequestContext currentContext = RequestContext.getCurrentContext();
//        currentContext.get(FilterConstants.SERVICE_ID_KEY);
        String serviceId = (String) currentContext.get("serviceId");

        return "item-service".equals(serviceId);

    }

    //过滤代码
    @Override
    public Object run() throws ZuulException {
        // 获得request对象
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        // 接收token参数
        String token = request.getParameter("token");
        // 如果没有token，阻止继续访问，直接返回响应
        if(StringUtils.isBlank(token)){
            // 阻止继续访问
            currentContext.setSendZuulResponse(false);
            //直接返回响应
            currentContext.addZuulResponseHeader("Content-Type", "text/html;charset=UTF-8");
            currentContext.setResponseBody("Not Login! 未登录！");
        }
        // 这个返回值不起任何作用，返回任何数据都可以
        return null;
    }
}
